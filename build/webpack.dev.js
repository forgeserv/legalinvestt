var webpack = require('webpack')
var merge = require('webpack-merge');
var baseWebpackConfig = require('./webpack.base');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = merge(baseWebpackConfig, {
  devtool: 'cheap-module-eval-source-map',
	module: {
		rules: [
			{
				test: /\.scss$/,
				exclude: /(node_modules)/,
				use: [
					'style-loader',
					'css-loader',
					'resolve-url-loader',
					{
						loader: 'sass-loader',
						options: {
							data: "@import '~@/stylesheets/core.scss';",
							includePaths: [
								require('bourbon-neat').includePaths,
							]
						}
					},
				]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify('development')
			}
		})
	]
})

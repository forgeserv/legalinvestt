'use strict'
const path = require('path')
const webpack = require('webpack')

require('dotenv').config()

module.exports = {
	entry: {
		app: './themes/frontend/main.js'
	},
	output: {
		path: path.join(__dirname, '../themes/frontend/dist/js'),
		publicPath: process.env.BASE_URL,
		filename: 'app.js'
	},
	resolve: {
		extensions: ['.js'],
		alias: {
			'@': path.join(__dirname, '..', 'themes/frontend'),
			'~': path.join(__dirname, 'node_modules')
		}
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['babel-preset-env']
					}
				}
			},
			{
				test: /\.(woff2?|eot|ttf|otf|svg)(\?.*)?$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							publicPath: process.env.BASE_URL,
							outputPath: '../fonts/'
						}
					}
				]
			},
			{
				test: /\.(png|jpg|jpeg|gif)$/,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: '[name].[ext]',
							publicPath: process.env.BASE_URL,
							outputPath: '../images/'
						}
					}
				]
			}
		]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				'BASE_URL': JSON.stringify(process.env.BASE_URL)
			}
		}),
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		})
	]
}

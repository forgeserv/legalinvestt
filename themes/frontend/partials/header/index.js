import './style.scss'

$('.header__menu-button').on('click', function(e) {
  e.preventDefault();

  $(this).toggleClass('active');
  $('.header__menu').toggleClass('active');
});

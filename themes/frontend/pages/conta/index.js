import './style.scss'

$('#modal-delete, #modal-ok').on('show.li.modal', function (event, button) {
    var id = button.data('id');
    $(this).find('.modal-content .modal-options .ad-action a').each(function() {
        $(this).attr('data-request-data', `id:${id}`);
    });
});

$('#modal-delete, #modal-ok').on('hide.li.modal', function (event) {
    $(this).find('.modal-content .modal-options .ad-action a').each(function() {
        $(this).attr('data-request-data', '');
    });
});
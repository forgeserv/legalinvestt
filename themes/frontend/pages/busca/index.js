import './style.scss'

$('.ads-search .search__filter-button').on('click', function() {
  $('.ads-search .search__filter-form').toggleClass('active');
});

$('.ads-search .search__filter-cancel').on('click', function() {
  $('.ads-search .search__filter-form').removeClass('active');
})

$('.ads-search .search__paginate-list-item a').on('click', function(e){
  e.preventDefault();
  $(".ads-search #search-filter").attr("action", this.href);
  $(".ads-search #search-filter").submit();
})

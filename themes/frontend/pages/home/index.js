import './style.scss'

$('.search__filter-form-select-area').each(function() {
  var _this = $(this);
  
  _this.find('.option').on('click', function() {
    $('.search__filter-form-select-area .option').removeClass('active');
    
    $(this).addClass('active');

    $(this).parent().find('input[type=hidden]').val($(this).data('value')).trigger("change");
  });

});

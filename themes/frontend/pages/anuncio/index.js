import './style.scss'

$('.ad__sidebar-widget-form').on('submit', function(e) {
  e.preventDefault();

  $('.ad__sidebar-widget').toggleClass('active');
  $('.ad__sidebar-email').toggleClass('active');
});

$(".show-phone").each(function() {
  var _parent = $(this);

  $(this).find('a').on('click', function(e) {
    e.preventDefault();

    $(this).remove();

    _parent.find('.phone-number').html($(this).data("phone"));
  });
});

window.toggleFavoriteStar = function(star, data){
  if(parseInt(data)){
    $('.icon-star', star).addClass('active');
  } else {
    $('.icon-star', star).removeClass('active');    
  }
}
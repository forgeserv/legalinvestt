import './style.scss'

$('.adv__form-plan-radio').each(function() {

  $(this).find('input[type=radio]').on('change', function(e) {
    $('.adv__form-plan-radio').removeClass('active');
    $(e.target).parent().addClass('active');
  });

});
import './style.scss'

$('.help-search .search__filter-button').on('click', function() {
  $('.help-search .search__filter-form').toggleClass('active');
});

$('.help-search .search__filter-cancel').on('click', function() {
  $('.help-search .search__filter-form').removeClass('active');
})

$('.help-search .search__paginate-list-item a').on('click', function(e){
  e.preventDefault();
  $(".help-search #search-filter").attr("action", this.href);
  $(".help-search #search-filter").submit();
})

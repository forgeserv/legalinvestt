import './style.scss'

$('.adv__form-radio').each(function() {
  $(this).find('input[type=radio]').on('change', function(e) {
    $('.adv__form-radio').removeClass('active');
    $(e.target).parent().addClass('active');
  });

  $(this).on('mouseenter', function() {
    $(this).addClass('hover');
  });

  $(this).on('mouseleave', function() {
    if(!$(this).hasClass('active')) {
      $('.adv__form-radio').removeClass('hover');
    }
  });
});
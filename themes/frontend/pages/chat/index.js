import './style.scss'
import PerfectScrollbar from 'perfect-scrollbar'

if($('.chat').length > 0) {
  
  window.initConversation = function(){   
    if($('.chat-content-messages').length > 0) {
      var ChatMessages = new PerfectScrollbar('.chat-content-messages');
    }  
  
    if($('.chat-content-header-close').length > 0) {
      $('.chat-content-header-close').on('click', function() {
        $('.chat-sidebar-list-item').removeClass('active');
        $('.chat-content').removeClass('active');
        $('.chat.static').removeClass('active');
      });
    }
  }

  initConversation();

  if($('.chat-sidebar-list').length > 0) {
    var ChatSidebar = new PerfectScrollbar('.chat-sidebar-list');
    
    $(window).resize(function() {
      ChatSidebar.update();
    });
  }

  if($('.chat-sidebar-list-item').length > 0) {
    $('.chat-sidebar-list-item').each(function() {
      $(this).find('a').on('click', function(e) {
        e.preventDefault();
      });

      $(this).on('click', function() {
        $('.chat-sidebar-list-item').removeClass('active');
        $(this).addClass('active');

        $('.chat-content').addClass('active');
      });
    });
  }

  window.showConversation = function(listItem, data){
    
    if(listItem)
      $(".author-name", listItem).removeClass("has-messages");
    if(data.url)
      window.history.replaceState({html: data.content}, 'Legal Invest - Chat', data.url);

    $("#chat-box").html(data.content);
    $("#chat-box").addClass('active');
    $(".chat.static.bshdw").addClass('active');
    initConversation();
  }

  window.showNewConversationMessage = function(form, data){
    //reset message input
    form.content.value = "";
  
    //change list item contents
    if($('.chat-sidebar-list-item').length > 0) {
      $("#chat-list-item-" + data.id + " .author-date").html(data.date);
      $("#chat-list-item-" + data.id + " .author-message").html(data.content);
    }
    
    //append message to the messages box
    $("#chat-box .chat-content-messages-list").append(data.messageHTML);
  }
  
}
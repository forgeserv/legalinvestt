import './stylesheets/core.scss'
import './stylesheets/defaults.scss'
import './partials'
import './pages'

$('.modal-button').each(function() {
  $(this).on('click', function(e) {
    e.preventDefault();

    $($(this).data('target'))
    .addClass('active')
    .trigger('show.li.modal', [$(this)]);

    if($('.modal').hasClass('modal-video')) {
      $('.modal-play iframe').attr('src', "https://www.youtube.com/embed/" + $('.modal-video').data('id') + "?autoplay=1");
    }
  });
});

$('.modal-close-button').on('click', function(e) {
  e.preventDefault();

  if($('.modal').hasClass('modal-video')) {
    $('.modal-play iframe').attr('src', '');
  }
  
  $('.modal')
  .removeClass('active')
  .trigger('hide.li.modal');
});

window.showFlash = function(data){
  if(typeof(data) == "string")
    data = JSON.parse(data);
  
  for (var status in data){
    if(data[status]){
      data[status].forEach(function(message){
        $('body').append('<p class="flash-message static '+status+'">' + message +'</p>')
      })
    }
  }

  return false;
}
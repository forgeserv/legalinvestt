<?php namespace Qualitare\LegalInvest\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Index extends Controller
{
    public $pageTitle;
    public $logo;
    
    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qualitare.LegalInvest', 'main-menu-item', 'index');
        
    }

    public function index()
    {
        $this->pageTitle = "Legal Invest";
        $this->logo = url("plugins/qualitare/legalinvest/assets/images/logo.svg");
    }
}

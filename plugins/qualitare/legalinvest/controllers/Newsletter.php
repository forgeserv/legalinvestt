<?php namespace Qualitare\LegalInvest\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Newsletter extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qualitare.LegalInvest', 'main-menu-item', 'newsletter');
    }
}

<?php namespace Qualitare\LegalInvest\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class HelpCategories extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'  ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Qualitare.LegalInvest', 'main-menu-item', 'help-categories');
    }
}

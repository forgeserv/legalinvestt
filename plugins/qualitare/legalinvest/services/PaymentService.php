<?php 

namespace Qualitare\LegalInvest\Services;

use Qualitare\LegalInvest\Services\PaymentGateways\PagseguroGateway;

class PaymentService
{
	/**
	 * The payment gateway gateway
	 */
	protected $gateway;

	/**
	 * Create a new service instance.
	 *
	 * @return void
	 */
	public function __construct($gateway)
	{
		if ($gateway == 'pagseguro')
			$this->gateway = new PagseguroGateway();
	}

	/**
	 * Return the service's current payment gateway.
	 *
	 * @return GatewayInterface
	 */
	public function getGateway()
	{
		return $this->gateway;
	}

	/**
	 * Replace current gateway with a new one.
	 *
	 * @param GatewayInterface
	 * @return void
	 */
	public function setGateway(GatewayInterface $gateway)
	{
		$this->gateway = $gateway;
	}

	/**
	 * Authorizes and submit checkout.
	 *
	 * @param  
	 * @return Response
	 */
	public function checkout($user, $items, $order)
	{
		// Process and get the payment register
		return $this->gateway->purchase($user, $items, $order->reference);
	}

}

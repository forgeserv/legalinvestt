<?php 

namespace Qualitare\LegalInvest\Services\PaymentGateways;

use PagSeguro\Library;
use PagSeguro\Configuration\Configure;
use PagSeguro\Services\Session;
use PagSeguro\Services\Application;
use PagSeguro\Domains\Requests\Payment;
use PagSeguro\Domains\Item;
use PagSeguro\Enum\Shipping;
use PagSeguro\Enum\PaymentMethod;
use PagSeguro\Services\Transactions;
use Illuminate\Http\Request;

class PagseguroGateway implements GatewayInterface
{

	public $notificationUrl = null;
	public $redirectUrl = null;
	public $credentials;

	/**
	 * Create a new gateway instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$config = config('payment.gateways.pagseguro');

		if(!$config)
			throw new \Exception('Not configured');

		// Check if library was already initialized
		if (!defined('PS_BASEPATH'))
			Library::initialize();

		// Set environment to sandbox or production
		Configure::setEnvironment($config['enviroment']);

		// If seller profile flow
		Configure::setAccountCredentials(
			$config['email'], $config['token']
		);

		$this->credentials = Configure::getAccountCredentials();

		$appUrl = config('app.url');
		$this->notificationUrl = $appUrl . $config['notificationUrl'];
		
	}

	/**
	 * Retrieves access token.
	 *
	 * @return string $access_token
	 */
	public function authorize()
	{
		// Get seller credentials
		$credentials = Configure::getAccountCredentials();

		// Generate session code
		$session = Session::create($credentials);

		// Return session code
		return $session->getResult();
	}

	/**
	 * Purchase items from a invoice.
	 *
	 * @param  string  $code
	 * @return \PagSeguro\Parsers\Transaction\Response
	 */
	public function purchase($user, $items, $reference)
	{
		$payment = new Payment();
		
        $payment->setSender()->setName($user->fullname);
		$payment->setSender()->setEmail($user->email);
        $payment->setSender()->setPhone($user->attributes['phone']); //phone without masks
        $payment->setSender()->setDocument()->withParameters('CPF', $user->cpf);

		$psItems = [];
		foreach ($items as $item){
			$psItem = new Item();
			
			$psItem->setId($item['id']);
			$psItem->setDescription($item['name']);
			$psItem->setAmount($item['price']);
			$psItem->setQuantity(1);
			$psItem->setShippingCost(0);
			$psItem->setWeight(0);

			$psItems[] = $psItem; 
		}

		$payment->setItems($psItems);

		//IMPORTANT!
        $payment->setReference($reference);
		$payment->setNotificationUrl($this->notificationUrl);

		$payment->setRedirectUrl($this->redirectUrl);
		
        $payment->setCurrency('BRL');
		
		// Add a group and/or payment methods name
		$payment->acceptPaymentMethod()->groups(
			PaymentMethod\Group::CREDIT_CARD,
			PaymentMethod\Group::BALANCE,
			PaymentMethod\Group::BOLETO,
			PaymentMethod\Group::DEPOSIT
		);

		// Itau
		$payment->acceptPaymentMethod()
			->name(PaymentMethod\Name::DEBITO_ITAU);

		// Bradesco
		$payment->acceptPaymentMethod()
			->name(PaymentMethod\Name::DEBITO_BRADESCO);

		// Banco do Brasil
		$payment->acceptPaymentMethod()
			->name(PaymentMethod\Name::DEBITO_BANCO_BRASIL);

		try {
			return $payment->register($this->credentials, true);
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * Get transaction data by code.
	 *
	 * @param  string  $code
	 * @return \PagSeguro\Parsers\Transaction\Response
	 */
	public function getTransaction($code)
	{
		return Transactions\Search\Code::search(
			Configure::getAccountCredentials(), $code
		);
	}

	/**
	 * Get converted transaction status.
	 *
	 * @param  string  $code
	 * @return string  pending | accepted | rejected | null
	 */
	public function getTransactionStatus($code)
	{
		$transaction = $this->getTransaction($code);

		return $this->resolveStatus($transaction->getStatus());
	}

	/**
	 * Get notification data by code.
	 *
	 * @param  string  $code
	 * @return \PagSeguro\Parsers\Transaction\Response
	 */
	public function getNotification()
	{
		return \PagSeguro\Services\Transactions\Notification::check(
			Configure::getAccountCredentials()
		);
	}

	public function resolveStatus($value) {
		// Translate PagSeguro status codes
		switch ($value) {
		case 1:
		case 2:
		case 5:
			return 'pending';
			break;
		case 3:
		case 4:
			return 'accepted';
			break;
		case 6;
		case 7:
			return 'canceled';
			break;
		default:
			return null;
			break;
		}
	}

	public function handleCallback(Request $request)
	{
		// Get transaction by code
		$transaction = $this->getNotification();

		return [
			'status' => $this->resolveStatus($transaction->getStatus()),
			'reference' => $transaction->getReference()
		];
	}
}

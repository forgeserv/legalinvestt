<?php namespace Qualitare\LegalInvest\Services\PaymentGateways;

use Illuminate\Http\Request;

interface GatewayInterface
{
	public function authorize();
	public function purchase($user, $items, $reference);
	public function getTransaction($code);
	public function getTransactionStatus($code);
	public function resolveStatus($value);
	public function handleCallback(Request $request);
}


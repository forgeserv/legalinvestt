<?php namespace Qualitare\LegalInvest;

use Route;
use Validator;
use Session; 

use Cms\Classes\Controller as Controller;
use RainLab\User\Controllers\Users as UsersController;

use RainLab\User\Models\User as UserModel;
use Qualitare\LegalInvest\Models\User as User;
use Qualitare\LegalInvest\Models\Ad as AdModel;

use Qualitare\LegalInvest\Services\PaymentService;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public $require = ['RainLab.User'];

    public function boot(){

        Validator::extend('phone', 'Propaganistas\LaravelPhone\Validation\Phone@validate', 'Formato de telefone inválido');

        UserModel::extend(function($model){
            User::extending($model);
        });

        UsersController::extendFormFields(function($form, $model, $context){

            if(!$model instanceof UserModel)
                return;

            $form->addFields(
                [
                    'birthday' => [
                        'label' => 'Data de nascimento',
                        'type'  => 'datepicker',
                        'mode'  => 'date',
                    ],
                    'cpf' => [
                        'label' => 'CPF',
                        'type'  => 'text'
                    ],
                    'phone' => [
                        'label' => 'Telefone',
                        'type'  => 'text'
                    ],
                ]
            );
        });

        Route::group(['middleware' => 'web'], function(){

            //RESTRICT ACCESS
            Route::group(['middleware' => 'Qualitare\LegalInvest\Classes\AuthMiddleware'], function () {
                
                Route::any('/conta/toggle-favorite/{id}', 
                    [
                        'as' => 'id', 
                        'uses' => 'Qualitare\LegalInvest\Components\Account@onToggleFavorite'
                    ]
                );

                Route::any('/conta/{slug}/{status?}', function($slug, $status = null){
                    $cmsController = new Controller();
                    return $cmsController->run("conta/$slug/$status");
                });
                
                Route::any('anuncio/criar', function() {
                    $cmsController = new Controller();
                    return $cmsController->run("anuncio/criar");
                });
                    
                Route::any('anuncio/editar/{id}', function($id) {
                    $cmsController = new Controller();
                    return $cmsController->run("anuncio/editar/$id");
                });

                Route::any('anuncio/turbinar/{id}', function($id) {
                    $cmsController = new Controller();
                    return $cmsController->run("anuncio/turbinar/$id");
                });

                Route::any('anuncio/remover','Qualitare\LegalInvest\Components\Ads@onRemoveAd');

                Route::any('anuncio/fechar/{status}', 
                    [
                        'as' => 'status', 
                        'uses' => 'Qualitare\LegalInvest\Components\Ads@onCloseAd'
                    ]
                );

                Route::any('/anuncio/{slug}/chat', function($slug) {
                    $cmsController = new Controller();
                    return $cmsController->run("anuncio/$slug");
                });  
                
                Route::any('/anuncio/{slug}', function($slug) {
                    $cmsController = new Controller();
                    return $cmsController->run("anuncio/$slug");
                });

                Route::any('/chat/{id?}', function($id = null) {
                    $cmsController = new Controller();
                    return $cmsController->run("chat/$id");
                });
                         
            });

            Route::group(['middleware' => 'Qualitare\LegalInvest\Classes\UserMiddleware'], function () {
                Route::any('/cadastrar', function() {
                    $cmsController = new Controller();
                    return $cmsController->run("cadastrar");
                });
                
                Route::any('/entrar', function() {
                    $cmsController = new Controller();
                    return $cmsController->run("entrar");
                });

                Route::any('/recuperarsenha', function() {
                    $cmsController = new Controller();
                    return $cmsController->run("recuperarsenha");
                });

                Route::any('/novasenha', function() {
                    $cmsController = new Controller();
                    return $cmsController->run("novasenha");
                });

                Route::post(
                    '/payment/{gateway}/notification', 'Qualitare\LegalInvest\Components\Payment@onNotification'
                )->where('gateway', 'pagseguro');
            });

        });
    }

    public function registerComponents()
    {
        return [
            'Qualitare\LegalInvest\Components\Register'         => 'register',
            'Qualitare\LegalInvest\Components\Login'            => 'login',
            'Qualitare\LegalInvest\Components\Account'          => 'account',
            'Qualitare\LegalInvest\Components\Ads'              => 'ads',
            'Qualitare\LegalInvest\Components\AdDetails'        => 'adDetails',
            'Qualitare\LegalInvest\Components\Newsletter'       => 'newsletter',
            'Qualitare\LegalInvest\Components\ContactForm'      => 'contactform',
            'Qualitare\LegalInvest\Components\ConsultingForm'   => 'consultingform',
            'Qualitare\LegalInvest\Components\Search'           => 'search',
            'Qualitare\LegalInvest\Components\Chat'             => 'chat',
            'Qualitare\LegalInvest\Components\HelpPostDetails'  => 'helpPostDetails',
            'Qualitare\LegalInvest\Components\HelpSearch'       => 'helpsearch',
            'Qualitare\LegalInvest\Components\Payment'          => 'payment',
            'Qualitare\LegalInvest\Components\Featured'         => 'featured'
        ];
    }

    public function registerMarkupTags()
    {
        return [
            'filters' => [
                'adStatusClass' => ['Qualitare\LegalInvest\Classes\AdFilters', 'statusClass'],
                'adStatusName'  => ['Qualitare\LegalInvest\Classes\AdFilters', 'statusName'],
                'chatDate'      => ['Qualitare\LegalInvest\Classes\ChatFilters', 'chatDate'],
                'chatSide'      => ['Qualitare\LegalInvest\Classes\ChatFilters', 'chatSide']
            ]
        ];
    }

    public function registerListColumnTypes()
    {
        return [
            'adstatusname' => function($value) { 
                return AdModel::getStatusNameByCode($value);
            },
            'orderstatusname' => function($value) { 
                //to do: get gateway name dinamically
                $service = new PaymentService('pagseguro');
                $gateway = $service->getGateway();
                
                return $gateway->resolveStatus($value);
            }
        ];
    }

}

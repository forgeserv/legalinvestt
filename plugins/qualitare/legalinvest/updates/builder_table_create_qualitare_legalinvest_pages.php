<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestPages extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_pages', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug', 45);
            $table->string('title', 255);
            $table->text('heading_title');
            $table->text('heading_text');
            $table->text('text');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_pages');
    }
}

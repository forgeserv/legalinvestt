<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestAdCategories extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_ad_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('parent_id');
            $table->string('slug', 255);
            $table->string('name', 255);
            $table->string('description', 255);
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_ad_categories');
    }
}

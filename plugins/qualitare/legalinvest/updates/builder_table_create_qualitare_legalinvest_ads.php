<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestAds extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_ads', function($table)
        {
            $defaultInt = 0;
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('subcategory_id');
            $table->unsignedInteger('state_id');
            $table->unsignedInteger('city_id');
            $table->string('slug', 255);
            $table->string('title', 255);
            $table->string('subtitle', 255)->nullable();
            $table->text('body');
            $table->unsignedInteger('status');
            $table->decimal('price_total', 10, 2);
            $table->decimal('price_partial', 10, 2);
            $table->unsignedInteger('views');
            $table->unsignedInteger('views_phone');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamp('suspended_at')->nullable();
            $table->timestamp('closed_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_ads');
    }
}
<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateMessagesTable extends Migration
{
	public function up()
	{
		Schema::create('qualitare_legalinvest_messages', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->text('content');
			$table->unsignedInteger('conversation_id');
			$table->unsignedInteger('user_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('qualitare_legalinvest_messages');
	}
}

<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestAdBoosts extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_ad_boosts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('ad_id');
            $table->unsignedInteger('boost_id');
            $table->date('date');
            $table->timestamp('boosted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_ad_boosts');
    }
}

<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestNewsletter extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_newsletter', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('created_at')->useCurrent();
            $table->string('email');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_newsletter');
    }
}

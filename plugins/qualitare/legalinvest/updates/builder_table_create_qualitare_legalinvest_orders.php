<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestOrders extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('ad_id');
            $table->integer('boost_id');
            $table->string('gateway');
            $table->integer('status');
            $table->string('reference')->nullable();
            $table->string('payment_code')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_orders');
    }
}

<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Faker;

use Qualitare\LegalInvest\Models\HelpPost as HelpPost;

class HelpPostsSeeder extends Seeder
{
    public function run()
    {
		$faker = Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $title = $faker->unique()->sentence(10);

            HelpPost::create([
                'help_category_id' => $faker->numberBetween(1,4),
                'title' => $title,
                'slug'  => str_slug(str_limit($title, 15), "-"),
                'text'  => $faker->paragraphs(3, true)
            ]);
        }

    }
}
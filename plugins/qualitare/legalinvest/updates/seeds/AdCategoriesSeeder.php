<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Faker;
use Qualitare\LegalInvest\Models\AdCategory as AdCategory;

class AdCategoriesSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        
        $categories = [
            [
                'id' => 1, 
                'slug' => 'precatorio',         
                'name' => 'Precatório',         
                'parent_id' => 0, 
                'description' => $faker->text(255)
            ],
            [
                'id' => 2, 
                'slug' => 'credito-condominal', 
                'name' => 'Crédito condominal', 
                'parent_id' => 0, 
                'description' => $faker->text(255)
            ],
            [
                'id' => 3, 
                'slug' => 'cessoes-credito',    
                'name' => 'Cessões de crédito', 
                'parent_id' => 0, 
                'description' => $faker->text(255)
            ],
            [
                'id' => 4, 
                'slug' => 'titulos-credito',    
                'name' => 'Títulos de crédito', 
                'parent_id' => 0, 
                'description' => $faker->text(255)
            ],
            [
                'id' => 5, 
                'slug' => 'titulos-judiciais',  
                'name' => 'Títulos judiciais',  
                'parent_id' => 0, 
                'description' => $faker->text(255)
            ],
            [
                'id' => 6, 
                'slug' => 'cessoes-direito',    
                'name' => 'Cessões de direito', 
                'parent_id' => 0, 
                'description' => $faker->text(255)
            ],
            [
                'id' => 7, 
                'slug' => 'federal',            
                'name' => 'Federal',            
                'parent_id' => 1, 
                'description' => $faker->text(255)
            ]
        ];

        foreach($categories as $category)
            AdCategory::create($category);
        
    }
}
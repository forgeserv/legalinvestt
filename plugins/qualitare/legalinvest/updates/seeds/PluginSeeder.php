<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;

class PluginSeeder extends Seeder {

	public function run() {
		// if (env('APP_ENV') != 'testing' && env('APP_ENV') != 'development')
		// 	return;

		$this->call([
			UsersSeeder::class,
			AdCategoriesSeeder::class,
			StatesSeeder::class,
			StateCitiesSeeder::class,
			AdsSeeder::class,
			MessagesSeeder::class,
			HelpCategoriesSeeder::class,
			HelpPostsSeeder::class,
			BoostsSeeder::class
		]);
	}

}

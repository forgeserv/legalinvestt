<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Qualitare\LegalInvest\Models\HelpCategory as HelpCategory;

class HelpCategoriesSeeder extends Seeder
{
    public function run()
    {
        $categories = [
            ['slug' => 'legal',   'name' => 'Legal'],
            ['slug' => 'vendas',  'name' => 'Vendas'],
            ['slug' => 'compras', 'name' => 'Compras'],
            ['slug' => 'conta',   'name' => 'Conta']
        ];

        foreach($categories as $category)
            HelpCategory::create($category);
        
    }
}
<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Qualitare\LegalInvest\Models\Boost as Boost;

class BoostsSeeder extends Seeder
{
    public function run()
    {
        $boosts = [
            ['interval' => 3, 'multiple' => 3, 'featured' => 0, 'best' => 0, 'name' => 'Anúncio Prata',    'price' => '5.99' ],
            ['interval' => 1, 'multiple' => 5, 'featured' => 1, 'best' => 1, 'name' => 'Anúncio Ouro',     'price' => '8.99' ],
            ['interval' => 1, 'multiple' => 7, 'featured' => 1, 'best' => 0, 'name' => 'Anúncio Diamante', 'price' => '45.99']
        ];

        foreach($boosts as $boost)
            Boost::create($boost);
        
    }
}
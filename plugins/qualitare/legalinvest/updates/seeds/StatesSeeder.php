<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Qualitare\LegalInvest\Models\State;

class StatesSeeder extends Seeder
{
    public function run()
    {
        $states = [
            ['id' => 1,  'code' => 12, 'name' => 'Acre',                'uf' => 'AC', 'region' => 1],
            ['id' => 2,  'code' => 27, 'name' => 'Alagoas',             'uf' => 'AL', 'region' => 2],
            ['id' => 3,  'code' => 16, 'name' => 'Amapá',               'uf' => 'AP', 'region' => 1],
            ['id' => 4,  'code' => 13, 'name' => 'Amazonas',            'uf' => 'AM', 'region' => 1],
            ['id' => 5,  'code' => 29, 'name' => 'Bahia',               'uf' => 'BA', 'region' => 2],
            ['id' => 6,  'code' => 23, 'name' => 'Ceará',               'uf' => 'CE', 'region' => 2],
            ['id' => 7,  'code' => 53, 'name' => 'Distrito Federal',    'uf' => 'DF', 'region' => 5],
            ['id' => 8,  'code' => 32, 'name' => 'Espírito Santo',      'uf' => 'ES', 'region' => 3],
            ['id' => 9,  'code' => 52, 'name' => 'Goiás',               'uf' => 'GO', 'region' => 5],
            ['id' => 10, 'code' => 21, 'name' => 'Maranhão',            'uf' => 'MA', 'region' => 2],
            ['id' => 11, 'code' => 51, 'name' => 'Mato Grosso',         'uf' => 'MT', 'region' => 5],
            ['id' => 12, 'code' => 50, 'name' => 'Mato Grosso do Sul',  'uf' => 'MS', 'region' => 5],
            ['id' => 13, 'code' => 31, 'name' => 'Minas Gerais',        'uf' => 'MG', 'region' => 3],
            ['id' => 14, 'code' => 15, 'name' => 'Pará',                'uf' => 'PA', 'region' => 1],
            ['id' => 15, 'code' => 25, 'name' => 'Paraíba',             'uf' => 'PB', 'region' => 2],
            ['id' => 16, 'code' => 41, 'name' => 'Paraná',              'uf' => 'PR', 'region' => 4],
            ['id' => 17, 'code' => 26, 'name' => 'Pernambuco',          'uf' => 'PE', 'region' => 2],
            ['id' => 18, 'code' => 22, 'name' => 'Piauí',               'uf' => 'PI', 'region' => 2],
            ['id' => 19, 'code' => 33, 'name' => 'Rio de Janeiro',      'uf' => 'RJ', 'region' => 3],
            ['id' => 20, 'code' => 24, 'name' => 'Rio Grande do Norte', 'uf' => 'RN', 'region' => 2],
            ['id' => 21, 'code' => 43, 'name' => 'Rio Grande do Sul',   'uf' => 'RS', 'region' => 4],
            ['id' => 22, 'code' => 11, 'name' => 'Rondônia',            'uf' => 'RO', 'region' => 1],
            ['id' => 23, 'code' => 14, 'name' => 'Roraima',             'uf' => 'RR', 'region' => 1],
            ['id' => 24, 'code' => 42, 'name' => 'Santa Catarina',      'uf' => 'SC', 'region' => 4],
            ['id' => 25, 'code' => 35, 'name' => 'São Paulo',           'uf' => 'SP', 'region' => 3],
            ['id' => 26, 'code' => 28, 'name' => 'Sergipe',             'uf' => 'SE', 'region' => 2],
            ['id' => 27, 'code' => 17, 'name' => 'Tocantins',           'uf' => 'TO', 'region' => 1]
        ];

        foreach($states as $state)
            State::create($state);
    }
}
<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Faker;
use Seeder;
use Qualitare\LegalInvest\Models\Conversation;
use Qualitare\LegalInvest\Models\Message;
use Qualitare\LegalInvest\Models\User;

class MessagesSeeder extends Seeder
{

	public function run()
	{
		$faker = Faker\Factory::create();

		$conversations = [];
		// Create multiple conversations for each user
		for($c = 0; $c < 6; $c++){

			// Make sure there is no conversation between the users
			while(true){
				$onwerId = $faker->unique(true)->numberBetween(1,4); 
				$userId  = $faker->unique()->numberBetween(1,4);
				
				if(!in_array([$onwerId, $userId], $conversations) 
				&& !in_array([$userId, $onwerId], $conversations))
					break;
			}
			
			$conversations[] = [$onwerId, $userId];
						
			// Find entities
			$owner = User::find($onwerId);
			$user  = User::find($userId);
			
			//Create conversation
			$conversation = Conversation::create([
				'owner_id' => $owner->id,
				'user_id' => $user->id
			]);

			//Create some messages
			for ($m = 0; $m < 20; $m++) {
				Message::create([
					'content' => $faker->sentence(10),
					'user_id' => $owner->id,
					'conversation_id' => $conversation->id,
					'created_at'      => now()
				]);

				Message::create([
					'content' => $faker->sentence(10),
					'user_id' => $user->id,
					'conversation_id' => $conversation->id,
					'created_at'      => now()
				]);
			}
		}
	}
}

<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Faker;
use Qualitare\LegalInvest\Models\User as User;

class UsersSeeder extends Seeder
{
    public function run()
    {
		$faker = Faker\Factory::create('pt_BR');

        $users = User::withTrashed()->get();
        
        foreach($users as $user){
            $user->forceDelete();
        }
        
        for ($i = 1; $i < 5; $i++) {
            User::create([
                'id'       => $i,
                'name'     => $faker->name,
                'surname'  => $faker->lastname,
                'cpf'      => $faker->cpf,
                'birthday' => $faker->dateTimeThisCentury()->format('Y-m-d'),
                'phone'    => "558332351322",
                'email'    => $faker->unique()->email,
                'password' => '123456789',
                'password_confirmation' => '123456789',
            ]);
        }
    }
    
}
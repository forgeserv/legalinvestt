<?php namespace Qualitare\LegalInvest\Updates\Seeds;

use Seeder;
use Faker;
use Qualitare\LegalInvest\Models\Ad as Ad;

class AdsSeeder extends Seeder
{
    public function run()
    {
		$faker = Faker\Factory::create();
 
        for ($i = 0; $i < 10; $i++) {

            // status 0 - aguardando aprovacao
            $title = $faker->sentence(11);
            $ad = [
                'user_id'        => $faker->numberBetween(1,4), 
                'status'         => 0,
                'category_id'    => 6, // cessões de direito
                'subcategory_id' => 7, // federal
                'state_id'       => 15,
                'city_id'        => 1338,
                'slug'           => Ad::setSlug($title),
                'title'          => $title,
                'price_partial'  => $faker->randomFloat(2, 2000, 3000),
                'price_total'    => $faker->randomFloat(2, 3000, 4000),
                'subtitle'       => $faker->sentence(20),
                'body'           => $faker->paragraphs(3, true),
                'views'          => 0,
                'views_phone'    => 0,
                'created_at'     => $faker->dateTimeBetween('-70 days', '-50 days')
            ];

            Ad::create($ad);

            // status 1 - publicado
            $title = $faker->sentence(11);
            $ad = [
                'user_id'        => $faker->numberBetween(1,4), 
                'status'         => 1,
                'published_at'   => $faker->dateTimeBetween('-50 days', '-20 days'),
                'category_id'    => 6, // cessões de direito
                'subcategory_id' => 7, // federal
                'state_id'       => 15,
                'city_id'        => 1338,
                'slug'           => Ad::setSlug($title),
                'title'          => $title,
                'price_partial'  => $faker->randomFloat(2, 2000, 3000),
                'price_total'    => $faker->randomFloat(2, 3000, 4000),
                'subtitle'       => $faker->sentence(20),
                'body'           => $faker->paragraphs(3, true),
                'views'          => 0,
                'views_phone'    => 0,
                'created_at'     => $faker->dateTimeBetween('-70 days', '-50 days')
            ];

            Ad::create($ad);

            // status 2 - suspenso
            $title = $faker->sentence(11);
            $ad = [
                'user_id'        => $faker->numberBetween(1,4), 
                'status'         => 2,
                'published_at'   => $faker->dateTimeBetween('-50 days', '-20 days'),
                'suspended_at'   => $faker->dateTime('-20 days'),
                'category_id'    => 6, // cessões de direito
                'subcategory_id' => 7, // federal
                'state_id'       => 15,
                'city_id'        => 1338,
                'slug'           => Ad::setSlug($title),
                'title'          => $title,
                'price_partial'  => $faker->randomFloat(2, 2000, 3000),
                'price_total'    => $faker->randomFloat(2, 3000, 4000),
                'subtitle'       => $faker->sentence(20),
                'body'           => $faker->paragraphs(3, true),
                'views'          => 0,
                'views_phone'    => 0,
                'created_at'     => $faker->dateTimeBetween('-70 days', '-50 days')
            ];

            Ad::create($ad);

            // status 3 - fechado
            $title = $faker->sentence(11);
            $ad = [
                'user_id'        => $faker->numberBetween(1,4), 
                'status'         => 3,
                'published_at'   => $faker->dateTimeBetween('-50 days', '-20 days'),
                'closed_at'      => $faker->dateTime('-20 days'),
                'category_id'    => 6, // cessões de direito
                'subcategory_id' => 7, // federal
                'state_id'       => 15,
                'city_id'        => 1338,
                'slug'           => Ad::setSlug($title),
                'title'          => $title,
                'price_partial'  => $faker->randomFloat(2, 2000, 3000),
                'price_total'    => $faker->randomFloat(2, 3000, 4000),
                'subtitle'       => $faker->sentence(20),
                'body'           => $faker->paragraphs(3, true),
                'views'          => 0,
                'views_phone'    => 0,
                'created_at'     => $faker->dateTimeBetween('-70 days', '-50 days')
            ];
            
            Ad::create($ad);
        }
    }
}
<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUserDetailsFields extends Migration
{
    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->date('birthday')->nullable();
            $table->string('cpf')->nullable();
            $table->string('phone')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn(['birthday', 'cpf', 'phone']);
        });  
    }
}

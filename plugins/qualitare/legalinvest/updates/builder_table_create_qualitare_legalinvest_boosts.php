<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestBoosts extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_boosts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 255);
            $table->unsignedInteger('best')->default(0);
            $table->unsignedInteger('interval');
            $table->unsignedInteger('multiple');
            $table->unsignedInteger('featured')->default(0);
            $table->decimal('price', 10, 2);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_boosts');
    }
}

<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestUserFavorites extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_user_favorites', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('ad_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_user_favorites');
    }
}

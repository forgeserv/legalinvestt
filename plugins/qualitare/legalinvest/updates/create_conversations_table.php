<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateConversationsTable extends Migration
{
	public function up()
	{
		Schema::create('qualitare_legalinvest_conversations', function(Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
            $table->unsignedInteger('owner_id');
            $table->unsignedInteger('user_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('qualitare_legalinvest_conversations');
	}
}

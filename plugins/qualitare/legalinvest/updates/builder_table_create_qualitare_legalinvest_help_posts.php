<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestHelpPosts extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_help_posts', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('help_category_id');
            $table->string('slug', 255);
            $table->string('title', 255);
            $table->text('text');
            $table->unsignedInteger('views')->default(0);
            $table->unsignedInteger('helped')->default(0);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_help_posts');
    }
}

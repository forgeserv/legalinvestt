<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestHelpCategories extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_help_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug', 255);
            $table->string('name', 255);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_help_categories');
    }
}

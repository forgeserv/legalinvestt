<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestStates extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_states', function($table)
        {
            $table->engine = 'InnoDB';
            $table->unsignedInteger('id');
            $table->unsignedInteger('code');
            $table->unsignedInteger('region');
            $table->string('name');
            $table->string('uf');
            $table->primary(['id']);
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_states');
    }
}

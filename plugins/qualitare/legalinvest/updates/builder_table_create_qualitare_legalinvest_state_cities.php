<?php namespace Qualitare\LegalInvest\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateQualitareLegalinvestStateCities extends Migration
{
    public function up()
    {
        Schema::create('qualitare_legalinvest_state_cities', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('state_id');
            $table->unsignedInteger('code');
            $table->string('name');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('qualitare_legalinvest_state_cities');
    }
}

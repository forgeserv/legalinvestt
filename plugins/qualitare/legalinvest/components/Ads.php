<?php namespace Qualitare\LegalInvest\Components;

use Auth;
use Db;
use Response;
use Redirect;
use Session;
use Cms\Classes\ComponentBase;
use Qualitare\LegalInvest\Models\Ad as Ad;
use Qualitare\LegalInvest\Models\AdCategory as AdCategory;
use Qualitare\LegalInvest\Models\Boost as Boost;
use Qualitare\LegalInvest\Models\State as State;
use Qualitare\LegalInvest\Models\StateCity as StateCity;

class Ads extends ComponentBase
{

    protected $user;
    public $id;
    public $ad;

    public function componentDetails()
    {
        return [
            'name'        => 'Anúncio da conta',
            'description' => 'Implementa a criação e edição de um anúncio'
        ];
    }

    public function defineProperties()
    {
        return [
            'acao' => [
                'title'             => 'Ação',
                'description'       => 'Criar ou editar',
                'default'           => '{{:acao}}',
                'type'              => 'string',
                'validationPattern' => '^(criar|editar)+$'
            ],
            'id' => [
                'type' => 'string',
                'default' => '{{:id}}'
            ]
        ];
    }
    
    public function init()
    {
        $this->user = Auth::getUser();
        $this->id = $this->property('id');
        
        if($this->id){
            $this->ad = Ad::findByUser($this->id, $this->user->id);
        }
    }

    public function onRun()
    {   
        if($this->id && !$this->ad)
            return Redirect::to('/error');
    
        $this->addJs('/modules/system/assets/ui/storm-min.js', 'core');
    }
    
    public function ad()
    {
        return $this->ad;
    }
    
    public function categories()
    {
       return AdCategory::where('parent_id', 0)
        ->orderBy('name', 'asc')
        ->get();
    }
    
    public function subcategories()
    {
      return Db::table('qualitare_legalinvest_ad_categories')
        ->where('parent_id', '>', 0)
        ->orderBy('name', 'asc')
        ->get();
    }

    public function boosts()
    {
        return Boost::all();
    }

    //return states on select2 format
    public function onGetStateOptions()
    {
        $data = post();
        $states = State::where('name', 'LIKE', '%'.$data['term'].'%')
        ->orWhere('uf', 'LIKE', '%'.$data['term'].'%')
        ->orderBy('name', 'asc')
        ->get(['id AS id', 'name as text'])
        ->toArray();

        return ["results" => $states];
    } 

    //return cities by selected state on select2 format
    public function onGetCityOptions()
    {
        $data = post();

        if(empty($data['state_id'])){
            return [];
        }

        $cities = StateCity::where('name', 'LIKE', '%'.$data['term'].'%')
        ->where('state_id', $data['state_id'])
        ->orderBy('name', 'asc')
        ->get(['id AS id', 'name as text'])
        ->toArray();

        return ["results" => $cities];
    } 

    public function onSave()
    {
        $data = post();

        $model = $this->ad ? $this->ad : new Ad();
        $model->fill($data);
        
        if(!$this->ad)
            $model->fillDefault();
            
        $model->save();

        $this->ad = $model;
        
        if(!$this->id){
            return Redirect::to('/anuncio/turbinar/' . $this->ad->id);
        }

        if($this->ad->status == 0)
            return Redirect::to('anunciar-aguardando');

        return Redirect::to('/anuncio/' . $this->ad->slug);
    }

    /**
     * Closes an ad
     *
     * @param  integer $status The new status
     * @return boolean Indicates if the ad was closed
     */ 
    public function onCloseAd($status){
        $data = post();
        
        if(Ad::close($data['id'], $status)){
            return Response::make(1, 200);
        }

        return Response::make(0, 200);
    }
    
    /**
     * Soft deletes an ad
     *
     * @return boolean Indicates if the ad was deleted
     */ 
    public function onRemoveAd(){
        $data = post();

        if(Ad::remove($data['id'])){
            return Response::make(1, 200);
        }
        
        return Response::make(0, 200);
    }
}

<?php namespace Qualitare\LegalInvest\Components;

use Flash; 

use Cms\Classes\ComponentBase;
use Qualitare\LegalInvest\Models\HelpPost;

class HelpPostDetails extends ComponentBase
{

    public $slug;
    public $id = null;
    public $record = null;
    
    public function componentDetails()
    {
        return [
            'name'        => 'Informações do post da ajuda',
            'description' => 'Implementa a exibição de posts de ajuda e a contagem de visualizações e quantas vezes o post ajudou'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'type' => 'string',
            ],
        ];
    }

    public function init()
    {
        $this->slug = $this->property('slug');
        $this->record = HelpPost::where('slug', $this->property('slug'))->first();      
    }

    public function onRun()
    {
        if($this->record){
            $this->record->increment("views");
            $this->record->save();
        }
    }

    public function record()
    {
        return $this->record;
    }

    public function onHelped()
    {
        try {
            $this->record->increment("helped");
            $this->record->save();

            return Flash::success('Obrigada!');
        } catch (Exception $e){
            return Flash::error('Ops, algo deu errado.');
        }
    }

}

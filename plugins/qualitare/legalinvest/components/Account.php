<?php namespace Qualitare\LegalInvest\Components;

use Auth;
use Db;
use Response;
use Request;
use Qualitare\LegalInvest\Models\Ad as Ad;
use Qualitare\LegalInvest\Models\UserFavorite as UserFavorite;

class Account extends \RainLab\User\Components\Account
{
    public $user;
    public $statusSlug;

    public function componentDetails()
    {
        return [
            'name'        => 'Conta',
            'description' => 'Implementa o formulário e listagem dos dados da conta'
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'type' => 'string',
            ],
            'favoriteAdId' => [
                'type' => 'string',
            ]
        ];

    }
    
    public function init()
    {
        $this->user = Auth::getUser();
        $this->statusSlug = $this->property('status');
    }

    public function statusSlug()
    {
        return $this->statusSlug;
    }

    public function statusFilters()
    {
        return Ad::getStatusFilters();
    }

    public function ads()
    {
        $method = studly_case($this->property('slug'));
        return $this->$method();   
    }

    public function meusAnuncios()
    {
        $query = Ad::where('user_id', $this->user->id);
    
        $status = Ad::getStatusCodeBySlug($this->statusSlug);
        
        if(is_string($status)){
            $query->where('status', $status);
        }

        if(is_array($status)){
            $query->whereIn('status', $status);
        }

        return $query->latest()->get();
    }

    public function meusFavoritos()
    {
        return UserFavorite::where('user_id', $this->user->id)->latest()->get();
    }

    /**
     * Adds/removes a user favorite
     *
     * @param  integer $id The ad id
     * @return integer 0|1 Indicates if the ad is or not a user favorite
     */ 
    public function onToggleFavorite($id){
         
        //if ad exists
        $ad = Ad::where("id", $id)->first();
        
        if(!$ad)
            return Response::make(0, 200);
        
        $user = Auth::getUser();

        //user cannot favorite their own ads
        if($ad->user_id == $user->id)
            return Response::make(0, 200);

        //if ad favorite exists: delete
        $favorite = UserFavorite::where([['user_id', $user->id],['ad_id', $id]])->first();
        if($favorite){
            $favorite->delete();
            return Response::make(0, 200);
        } 

        //if ad favorite does not exist: create
        $newFav = UserFavorite::create([
            'ad_id' => $id, 
            'user_id' => $user->id,
            'created_at' => now()
        ]);

        if($newFav)
            return Response::make(1, 200);

        return Response::make(0, 200);
    }

}

<?php namespace Qualitare\LegalInvest\Components;

use Flash;
use Cms\Classes\ComponentBase;
use Qualitare\LegalInvest\Models\Ad as Ad;


class Featured extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Destaques',
            'description' => 'Exibe os destaques na página inicial'
        ];
    }

    public function ads()
    {
        return Ad::where('status', 1)
        ->orderBy("price_total", "desc")
        ->latest()
        ->limit(6)
        ->get();
    }
}

<?php namespace Qualitare\LegalInvest\Components;

use Auth;
use Response;
use Config;
use Request;
use Flash;
use Redirect;

use Qualitare\LegalInvest\Services\PaymentService;

use Qualitare\LegalInvest\Models\Ad;
use Qualitare\LegalInvest\Models\Boost;
use Qualitare\LegalInvest\Models\Order;

use Cms\Classes\ComponentBase;


class Payment extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Pagamento',
            'description' => 'Implementa os métodos de pagamento'
        ];
    }

    public function onProcess(){

        $user = Auth::getUser();
        $post = post();
        
        if(empty($post['gateway']))
            return Response::make('A gateway is needed', 400);

        if(!$ad = Ad::findByUser($post['ad_id'], $user->id) )
            return Response::make('Ad does not exist', 400);

        if(!$boost = Boost::find($post['boost_id']))
            return Response::make('Boost plan does not exist', 400);


        $order = Order::create([
            'user_id'  => $user->id,
            'ad_id'    => $ad->id,
            'boost_id' => $boost->id,
            'gateway'  => $post['gateway'],
            'status'   => 0
        ]);

        //generate a reference with the order id and a 'private key' and save
        $reference = md5($order->id . '$4sR#V!6^&eFz&N!');
        $order->reference = $reference;
        $order->save();

        try {
            $service = new PaymentService($post['gateway']);

            $items = [[
                'id'    => '1',
                'name'  => "$boost->name para o anúncio '" . substr($ad->title, 0, 10) . "'",
                'price' => $boost->price
            ]];
        
            $response = $service->checkout($user, $items, $order);
            $paymentCode = $response->getCode();
            
            $order->payment_code = $paymentCode; 
            $order->save();

            return Response::make($paymentCode, 200);

        } catch (\Exception $e) {
            $xml = simplexml_load_string($e->getMessage());
            $errorCode = $xml->error->code->__toString();
            
            switch($errorCode){
                case "11164":
                    $message = 'Formato de CPF do comprador informado é inválido'; break;
                default:
                    $message = 'Um erro ocorreu durante o processamento do seu pedido.'; break;
            }

            return Response::make(["error" => [$message]], 500);
        }
        
    }

    public function onOrdered()
    {
        $post = post();
        $user = Auth::getUser();
        $adId = $this->property("ad_id");

        if(!$ad = Ad::findByUser($adId, $user->id) )
            return Flash::error("O anúncio não existe");

        if(!$order = Order::where("payment_code", $post['code']) )
            return Flash::error("O pedido não existe");
        
        if($ad->status == 0)
            return Redirect::to('anunciar-aguardando-plano-andamento');

        return Redirect::to('anunciar-plano-andamento');
    }

    public function onAbort()
    {
        $post = post();
        
        if(!$order = Order::where("payment_code", $post['code']) )
            return Flash::error("O pedido não pode ser cancelado pois não existe");

        $order = Order::where("payment_code", $post['code']);
        $order->delete();
        
        return Flash::success("O pedido foi cancelado");
    }

    public function onNotification($gateway){

        //enviroment: developing/testing
        header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
        
        $service = new PaymentService($gateway);
        $gateway = $service->getGateway();

        $notification = $gateway->getNotification();
        $reference = $notification->getReference();
        
        if(!$order = Order::where('reference', $reference)->first())
            return Response::make('Order does not exist', 400);
        
        $order->status = $notification->getStatus();
        $order->save();

        if($order->isCanceled())
        {
            $order->delete();
        }

        if($order->isPaid())
        {
            //boost logic
        }

    }
}
<?php namespace Qualitare\LegalInvest\Components;

use Mail;
use Cms\Classes\ComponentBase;

class ConsultingForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Formulário de consultoria',
            'description' => 'Implementa o formulário de consultoria'
        ];
    }

    public function defineProperties()
    {
        return [
            'toEmail' => [
                'title'             => 'Email destino',
                'description'       => 'Email que receberá os pedidos de consultoria',
                'default'           => 'mariannactx@gmail.com',
                'type'              => 'string',
            ],
            'toName' => [
                'title'             => 'Nome destino',
                'description'       => 'Identificação do email que receberá os pedidos de consultoria',
                'default'           => 'Marianna',
                'type'              => 'string',
            ]
        ];
    }

    public function onSend()
    {
        $data = post();

		Mail::send('qualitare.legalinvest::mail.consultoria', [
            //
        ], function($message) use ($data) {
            $message->from($data['email']);
            $message->to($this->property('toEmail'), $this->property('toName'));
			$message->subject('Legal Invest - Consultoria');
        });
        
        return "Enviou!";
    }

}

<?php namespace Qualitare\LegalInvest\Components;

use Auth;
use Flash;
class Login extends \RainLab\User\Components\Account
{

    public function componentDetails()
    {
        return [
            'name'        => 'Entrar',
            'description' => 'Implementa o formulário de login'
        ];
    }

    public function onSignin()
    {
        try {
            return parent::onSignin();
        } catch (\October\Rain\Auth\AuthException $e) {
            $authMessage = $e->getMessage();
            // for error messages see October\Rain\Auth\Manager
            if (strrpos($authMessage,'hashed credential') !== false) {
                $message = 'Email e/ou senha estão incorretos';
            } elseif (strrpos($authMessage,'user was not found') !== false) {
                $message = 'Email e/ou senha estão incorretos';
            } elseif (strrpos($authMessage,'not activated') !== false) {
                $message = 'O usuário ainda não foi ativado. Verifique seu email ou entre em contato conosco';
            } else {
                $message = 'Ocorreu um erro ao fazer login';
            }

            return Flash::error($message);
        }

    }

}

<?php namespace Qualitare\LegalInvest\Components;

use Auth;
use Db;
use Redirect;
use Exception;
use Response;

use Cms\Classes\ComponentBase;

use Qualitare\LegalInvest\Models\Ad as Ad;
use Qualitare\LegalInvest\Models\UserFavorite as UserFavorite;

class AdDetails extends ComponentBase
{

    private $ad;
    public $user;

    public function componentDetails()
    {
        return [
            'name'        => 'Anúncio',
            'description' => 'Implementa a visualização de um anúncio'
        ];
    }

    public function init()
    {
        $this->user = Auth::getUser();
    }

    public function ad(){
        return $this->ad;
    }

    public function onRun()
    {
        try {
            $slug = $this->property('slug');
            
            $query = Ad::where("slug", $slug);
            
            if($this->user){
                $query->with("userfavorite");
            }

            $this->ad = $query->firstOrFail();
            
            //if not published, show 404 page
            if( $this->ad->status == 0 ){
                return Redirect::to('/anuncio-aguardando');            
            }
            
            //if suspended or closed, show removed page
            if($this->ad->status > 1){
                return Redirect::to('/anuncio-removido');
            }
            
            //prevent incrementing views if user is accessing their own ad 
            if($this->user && $this->ad->user_id != $this->user->id){
                $this->ad->increment("views");
                $this->ad->save();
            }
            
        } catch (Exception $e){
            return Redirect::to('/error');
        }
    }


    public function onViewPhone()
    {
        try {
            $this->ad = Ad::where([
                ["slug", $this->property('slug')],
                ["user_id", "<>", $this->user->id] //prevent incrementing views if user is accessing their own ad 
            ])->firstOrFail();

            $this->ad->increment("views_phone");
            $this->ad->save();

            return Response::make('Success', 200);
        } catch (Exception $e){
            return Response::make('Success', 200);
        }
    }

}

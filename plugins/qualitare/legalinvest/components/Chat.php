<?php namespace Qualitare\LegalInvest\Components;

use Auth;
use Response;
use Cms\Classes\ComponentBase;
use Qualitare\LegalInvest\Classes\ChatFilters;

use Qualitare\LegalInvest\Models\Conversation as Conversation;
use Qualitare\LegalInvest\Models\Message as Message;
use Qualitare\LegalInvest\Models\Ad as Ad;

class Chat extends ComponentBase
{

    protected $user;
    protected $all = null;
    protected $ad  = null;

    public function componentDetails()
    {
        return [
            'name'        => 'Chat',
            'description' => 'Implementa o chat'
        ];
    }

    public function defineProperties()
    {
        return [
            'id' => ['title'   => 'Conversation identification'],
            'slug' => ['title' => 'Ad identification']
        ];
    }

    public function init()
    {
        $this->user = Auth::getUser();
        
        if(!$this->user)
            return; 
            
        //get conversation by ad slug
        $slug = $this->property("slug");

        if($slug){
            
            //get the current ad data
            $ad = Ad::where([
                ['slug', $slug],
                ['user_id', '<>', $this->user->id] //avoid conversations within user own ads
            ])->first();

            if(!$ad)
                return false;
            
            $this->ad = $ad;

            $users = [$this->user->id, $this->ad->user_id];

            //get the conversation between the current user and the ad owner if it exists
            $conversation = Conversation::whereIn('owner_id', $users)
            ->whereIn('user_id', $users)
            ->first();

            if($conversation){
                $this->properties['id'] = $conversation->id;
            }
        }
    }
    
    public function all(){

        $conversations = Conversation::with(['latestMessage'])
            ->where('owner_id', $this->user->id)
            ->orWhere('user_id', $this->user->id)
            ->get();
        
        $conversations = $conversations->sortByDesc('latestMessage');

        if($conversations){
            
            $all = [];

            foreach($conversations as $conversation){
            
                $lastMessage = $conversation->latestMessage;
                
                if($lastMessage){
                    $currentId = $this->property("id");
                    
                    $lastIsFromUser = $lastMessage->user_id != $this->user->id;
                    $lastIsUnread   = !$lastMessage->updated_at;
                    $isCurrent      = $currentId == $conversation->id;
                
                    $hasUnread = $lastIsUnread && $lastIsFromUser && !$isCurrent;
                    
                    $otherUser = $conversation->owner_id == $this->user->id ? $conversation->user : $conversation->owner;

                    $all[$conversation->id] = [
                        'userName' => "$otherUser->name $otherUser->surname",
                        'lastMessage' => $lastMessage,
                        'hasUnread' => $hasUnread
                    ];
                }
            }

            $this->all = $all;
        }

        return $this->all;    
    }

    public function current(){
        $id = $this->property("id");
        
        if(!empty($id)){

            $conversation = Conversation::find($id);

            if($this->user->id != $conversation->owner_id && $this->user->id != $conversation->user_id)
                return null;

            Message::markAsReadByConversation($id);

            $otherUser = $conversation->owner_id == $this->user->id ? $conversation->user : $conversation->owner;
            
            return [
                'id' => $id,
                'userName' => "$otherUser->name $otherUser->surname",
                'messages' => Message::where("conversation_id", $id)->get()
            ];
        }

        return null;
    }

    public function onChatLoad()
    {
        $id = $this->property("id");
        $messagesBox = $this->renderPartial('chat/messagesbox.htm');
        return Response::make([
            'url'  => "/chat/$id",
            'content' => $messagesBox 
        ], 200);
    }

    public function onChatSend()
    {
        $id = $this->property("id");
        $data = post();

        //avoid empty messages
        if(empty($data['content']))
            return Response::make('Mensagem vazia', 400);

        //check if conversation exists
        $conversation = Conversation::where('id', $id)
        ->where(function ($query) {
            $query->where('owner_id', $this->user->id)
                  ->orWhere('user_id', $this->user->id);
        })->first();

        if(empty($conversation)){
            return Response::make('Conversa não existe', 400);
        }    

        $message = Message::create([
            'content'         => $data['content'],
            'user_id'         => Auth::getUser()->id,
            'conversation_id' => $id,
            'created_at'      => now()
        ]);

        $messageHTML = $this->renderPartial('chat/message.htm', ['message' => $message]);        
        return Response::make([
            "id" => $message->conversation_id,
            "date" => ChatFilters::chatDate($message->created_at),
            "content" => $message->content,
            "messageHTML" => $messageHTML
        ], 200);
    }

    public function onAdChatLoad()
    {
        if(!$this->ad)
            return Response::make(0, 400);

        //if there is no conversation yet, create a new one
        if(!$this->property('id')){
            $conversation = Conversation::create([
                'owner_id' => $this->user->id,
                'user_id'  => $this->ad->user_id
            ]);

            //set the property id
            $this->properties['id'] = $conversation->id;
        }
        
        $messagesBox = $this->renderPartial('chat/messagesbox.htm');
        return Response::make(['content' => $messagesBox], 200);
    }  

}

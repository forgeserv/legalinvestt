<?php namespace Qualitare\LegalInvest\Components;

use Cms\Classes\ComponentBase;

use Qualitare\LegalInvest\Models\Ad as Ad;
use Qualitare\LegalInvest\Models\AdCategory as AdCategory;
use Qualitare\LegalInvest\Models\State as State;
use Qualitare\LegalInvest\Models\StateCity as StateCity;

class Search extends ComponentBase
{
    public $filters;

    public function componentDetails()
    {
        return [
            'name'        => 'Cadastrar',
            'description' => 'Implementa o formulário de cadastro'
        ];
    }

    public function defineProperties()
    {
        return [
            'category_slug' => [
                'default' => '',
                'type'    => 'string',
            ]
        ];  
    }
        
    public function init(){
        $this->filters = post();
        
        if(!empty($this->property('category_slug'))){
            $category = AdCategory::where("slug", $this->property('category_slug'))->first();
 
            if($category)
                $this->filters['where']['category_id'] = $category->id;
        }
    }

    public function onRun(){
        $this->addJs('/modules/system/assets/ui/storm-min.js', 'core');
    }

    public function ads()
    {
        $model = new Ad();
        return $model->search($this->filters);
    }

    public function order(){
        return [
            1 => 'Relevância',
            2 => 'Menor preço de venda',
            3 => 'Maior preço de venda',
            4 => 'Menor valor do processo',
            5 => 'Maior valor do processo',
            6 => 'Menor diferença de valores (%)',
            7 => 'Maior diferença de valores (%)'
        ];
    }

    public function categories(){
        return AdCategory::where('parent_id', 0)
        ->orderBy('name', 'asc')
        ->get();
    }

    //return states on select2 format
    public function onGetStateOptions()
    {
        $data = post();
        $states = State::where('name', 'LIKE', '%'.$data['term'].'%')
        ->orWhere('uf', 'LIKE', '%'.$data['term'].'%')
        ->orderBy('name', 'asc')
        ->get(['id AS id', 'name as text'])
        ->toArray();

        return ["results" => $states];
    } 

    //return cities by selected state on select2 format
    public function onGetCityOptions()
    {
        $data = post();
        
        if(empty($data['where']['state_id'])){
            return ["results" => []];
        }

        $cities = StateCity::where('name', 'LIKE', '%'.$data['term'].'%')
        ->where('state_id', $data['where']['state_id'])
        ->orderBy('name', 'asc')
        ->get(['id AS id', 'name as text'])
        ->toArray();

        return ["results" => $cities];
    } 

    public function state(){
        if(empty($this->filters['where']) || empty($this->filters['where']['state_id']))
            return null;
        
        return State::where('id', $this->filters['where']['state_id'])->first();
    }

    public function city(){
        if(empty($this->filters['where']) || empty($this->filters['where']['city_id']))
            return null;
        
        return StateCity::where('id', $this->filters['where']['city_id'])->first();
    }

    public function priceRanges(){
        return [
          "0,10000" => "Até R$ 10.000",
          "10000,50000" => "R$ 10.000 a R$ 50.000",
          "50000,100000" => "R$ 50.000 a R$ 100.000",
          "100000,200000" => "R$ 100.000 a R$ 200.000",
          "200000,500000" => "R$ 200.000 a R$ 500.000",
          "500000,0" => "Acima de R$ 500.000",
        ];
    }
}

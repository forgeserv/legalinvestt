<?php namespace Qualitare\LegalInvest\Components;

use Flash;
use Validator;
use ValidationException;
use Cms\Classes\ComponentBase;
use Qualitare\LegalInvest\Models\Newsletter as NewsletterModel;


class Newsletter extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Newsletter',
            'description' => 'Armazena emails.'
        ];
    }

    public function onSave(){
        $data = post();
        
        $model = new NewsletterModel();

        $rules = $model->rules;

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $model->fill($data);
        $model->save();

        return Flash::success('Email cadastrado.');
    }
}

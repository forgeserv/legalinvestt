<?php namespace Qualitare\LegalInvest\Components;

use Cms\Classes\ComponentBase;
use Qualitare\LegalInvest\Models\HelpPost;

class HelpSearch extends ComponentBase
{
    public $filters;

    public function componentDetails()
    {
        return [
            'name'        => 'Busca posts de ajuda',
            'description' => 'Implementa a busca de posts de ajuda por termo'
        ];
    }

    public function defineProperties()
    {
        return [
            'category_slug' => [
                'default' => '',
                'type'    => 'string',
            ]
        ];  
    }
        
    public function init(){
        $this->filters = post();
    }

    public function posts()
    {
        $model = new HelpPost();
        return $model->search($this->filters);
    }

}

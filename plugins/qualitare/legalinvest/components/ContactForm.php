<?php namespace Qualitare\LegalInvest\Components;

use Mail;
use Response;
use Cms\Classes\ComponentBase;

class ContactForm extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'Formulário de contato',
            'description' => 'Implementa o formulário de contato'
        ];
    }

    public function defineProperties()
    {
        return [
            'toEmail' => [
                'title'             => 'Email destino',
                'description'       => 'Email que receberá os contatos',
                'default'           => 'mariannactx@gmail.com',
                'type'              => 'string',
            ],
            'toName' => [
                'title'             => 'Nome destino',
                'description'       => 'Identificação do email que receberá os contatos',
                'default'           => 'Marianna',
                'type'              => 'string',
            ],
            'description' => [
                'title'             => 'Descrição do formulário',
                'description'       => 'Texto de descrição abaixo do título',
                'default'           => '',
                'type'              => 'string',
            ],
        ];
    }

    public function onSend()
    {
        $data = post();
        
		Mail::send('qualitare.legalinvest::mail.contato', [
            "content" => $data['content']
        ], function($message) use ($data) {
            $message->from($data['email'], $data['name']);
            $message->to($this->property('toEmail'), $this->property('toName'));
			$message->subject('Legal Invest - Fale Conosco');
        });
        
        $mailSent = $this->renderPartial(
            'contato/enviado.htm', 
            ['sentMessage' => $this->property("sentMessage")]
        );

        return Response::make($mailSent, 200);
    }

}

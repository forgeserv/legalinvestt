<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class HelpCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_help_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'posts' => 'Qualitare\LegalInvest\Models\HelpPost'
    ];

    public $attachOne = [
        'icon' => 'System\Models\File'
    ];
}

<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class UserFavorite extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['created_at', 'deleted_at'];
    public $timestamps = false;
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_user_favorites';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $fillable = ['ad_id', 'user_id'];

    public $belongsTo = [
        'ad' => [
            'Qualitare\LegalInvest\Models\Ad', 
            'key' => 'ad_id'
        ],
        'user' => [
            'Qualitare\LegalInvest\Models\Ad', 
            'key' => 'user_id'
        ],
    ];

}

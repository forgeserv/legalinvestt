<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class StateCity extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_state_cities';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'state' => [
            'Qualitare\LegalInvest\Models\State',
            'key' => 'state_id'
        ]
    ];
    
}

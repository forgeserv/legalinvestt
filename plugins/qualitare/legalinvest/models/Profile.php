<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class Profile extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_profiles';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $attachOne = [
        'photo' => 'System\Models\File'
    ];

    public function getImageAttribute()
    {
        $profile = $this->find($this->id);
       
        if(is_null($profile->photo)){
            return ' ';
        }

        return '<img src="'.$profile->photo->getThumb(50, 50, 'crop').'" />';
    }
}

<?php

namespace Qualitare\LegalInvest\Models;

use Model;

class Conversation extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_conversations';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'owner_id',
        'user_id',
    ];

    /**
     * @var array Relations
     */
	public $hasMany = [
		'messages' => [Message::class]
	];

    public $hasOne = [
        'latestMessage' => [Message::class]
    ];

    public $belongsTo = [
        'owner' => ['Qualitare\LegalInvest\Models\User', 'key' => 'owner_id'],
        'user' => ['Qualitare\LegalInvest\Models\User', 'key' => 'user_id']
    ];

    public function latestMessage() {
        return $this->hasOne(Message::class)->orderBy('id', 'desc');
    }
}

<?php namespace Qualitare\LegalInvest\Models;

use Auth;
use Model;
use Exception;

use \Illuminate\Pagination\Paginator;
use \Illuminate\Pagination\LengthAwarePaginator;

/**
 * Model
 */
class Ad extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_ads';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user' => [
            'Qualitare\LegalInvest\Models\User', 
            'key' => 'user_id'
        ],
        'category' => [
            'Qualitare\LegalInvest\Models\AdCategory', 
            'key' => 'category_id'
        ],
        'subcategory' => [
            'Qualitare\LegalInvest\Models\AdCategory', 
            'key' => 'subcategory_id'
        ],
        'state' => [
            'Qualitare\LegalInvest\Models\State', 
            'key' => 'state_id'
        ],
        'city' => [
            'Qualitare\LegalInvest\Models\StateCity', 
            'key' => 'city_id'
        ],
    ];

    public $hasMany = [
        'userfavorite' => [
            'Qualitare\LegalInvest\Models\UserFavorite',
            'key' => 'ad_id',
            'other_key' => 'user_id'
        ],
        'boost' => [
            'Qualitare\LegalInvest\Models\Boost',
            'key' => 'ad_id',
        ]
    ];
    
    protected $with = ['category', 'subcategory'];

    protected $searchable = [
        'category_id', 
        'price_partial', 
        'price_total', 
        'title', 
        'subtitle',
        'body',
        'state_id',
        'city_id'
    ];

    protected $sortable = ['views', 'price_partial', 'price_total', 'price_diff'];

    protected $fillable = [
        'title',
        'subtitle',
        'body',
        'price_partial',
        'price_total',
        'category_id', 
        'subcategory_id', 
        'state_id',
        'city_id'
    ];

    protected $appends = ['price_diff'];

    public function getPriceDiffAttribute()
    {
        $diff = 100 - ( ($this->price_partial * 100)/$this->price_total );
        $nodecimals = number_format($diff, 0, '.', '');
        return "-$nodecimals%";
    }

    public static function findByUser($id, $user_id)
    {
        return self::where([ ['id',$id], ['user_id',$user_id] ])->first();
    }

    public static function getStatusCodeBySlug($slug)
    {
        switch($slug){
            case 'aguardando-aprovacao': return '0';
            case 'publicados': return '1';
            case 'suspensos': return '2';
            case 'finalizados': return ['3','4']; //vendeu por outros meios | pela legal invest
            default: return false;
        }
    }

    public static function getStatusFilters()
    {
        return [
            'todos'                => 'Todos',
            'aguardando-aprovacao' => 'Aguardando aprovação', //0
            'publicados'            => 'Publicados', //1                   
            'suspensos'             => 'Suspensos', //2
            'finalizados'           => 'Finalizados', //3,4
        ];
    }

    public static function getStatusNameByCode($code)
    {
        switch($code){
            case  '': return 'Todos';
            case '0': return 'Aguardando aprovação';
            case '1': return 'Publicado';
            case '2': return 'Suspenso';
            case '3': return 'Finalizado'; //vendeu por outros meios
            case '4': return 'Finalizado'; //vendeu pela legal invest
            case ['3', '4']: return 'Finalizado';
                
            default: return false;
        }
    }

    /**
     * Getter for the backend form usage
     */
    public static function getStatusOptions()
    {
        return [
            0 => 'Aguardando aprovação',
            1 => 'Publicado',
            2 => 'Suspenso'
        ];
    }

    public function beforeUpdate()
    {
        if($this->status != $this->original['status'] ){
            //publishing ad (status: 1)
            if($this->original['status'] == 0 && $this->status == 1){
                $this->published_at = now();
            }

            //suspending ad (status: 2)
            if($this->status == 2){
                $this->suspended_at = now();
            }

            //closing ad (status: 3,4)
            if(in_array($this->status, [3,4])){
                $this->closed_at = now();
            }
        }
    }

    public static function close($id, $status)
    {
        $ad = Ad::find($id);

        if($ad->user_id != Auth::getUser()->id)
            return false;

        if($ad->closed_at)
            return false;
            
        if($status == "outros"){
            $ad->status = 3;
        } else if($status == "legal-invest"){
            $ad->status = 4;
        }  

        $ad->closed_at = now();
        return $ad->save();
    }

    public static function remove($id)
    { 
        $ad = Ad::find($id);

        if(!$ad || $ad->user_id != Auth::getUser()->id)
            return false;

        return $ad->delete();
    }

    public function fillDefault(){
        $user = Auth::getUser();
        $this->user_id = $user->id;
        
        $this->slug = Ad::setSlug($this->title);
        $this->status = 0;
        $this->views = 0;
        $this->views_phone = 0;
    }

    public static function setSlug($title){
        return str_slug(str_limit($title, 15), "-");
    }

    public function userfavorite(){
        $user = Auth::getUser();
        return $this->hasOne(UserFavorite::class)->where("user_id", $user->id);
    }

    public function search($filters){
        
        $query = $this->newQuery();
        
        if($filters){
            
            /* 
             * order: (column,direction)
             * where
             * - category_id
             * - price_partial:  range(min,max)|min|max
             * - price_total:    range(min,max)|min|max
            */
            
            if(!empty($filters['order'])){
                $orderValues = [
                    1 => ['views',         'desc'],
                    2 => ['price_partial', 'asc' ],
                    3 => ['price_partial', 'desc'],
                    4 => ['price_total',   'asc' ],
                    5 => ['price_total',   'desc'],
                    6 => ['price_diff',    '2' ], //collection sortBy asc
                    7 => ['price_diff',    '1' ]   //collection sortBy desc
                ];
                
                $orderIndex = $filters['order'];

                $column    = $orderValues[$orderIndex][0];
                $direction = $orderValues[$orderIndex][1];

                if(in_array($column, $this->sortable)){

                    if(!in_array($column, $this->appends)){
                        $query->orderBy($column, $direction);
                    } else {
                        $sortBy = [$column, $direction];
                    }

                }
                
            }

            if(!empty($filters['where'])){
                $where = $filters['where'];

                if(!empty($where['term'])){

                    $query->where(function ($query) use ($where) {
                        $query->where(  'title',    'LIKE', "%". $where['term'] . "%")
                            ->orWhere('subtitle', 'LIKE', "%". $where['term'] . "%")
                            ->orWhere('body',     'LIKE', "%". $where['term'] . "%");
                    });

                    unset($where['term']);
                }

                foreach($where as $column => $value){
                    if(!in_array($column, $this->searchable))
                        continue;

                    if(!empty($value['range'])){
                        $values = explode(",", $value['range']);
                        $value['min'] = $value['min'] ? $value['min'] : $values[0];
                        $value['max'] = $value['max'] ? $value['max'] : $values[1];
                    }

                    if(!empty($value['min'])){
                        $query->where($column, '>=', $value['min']);
                    }
                    
                    if(!empty($value['max'])){
                        $query->where($column, '<=', $value['max']);
                    }

                    if(!empty($value) && !is_array($value)){
                        $query->where($column, $value);
                    }  
                }
            }
        }

        $ads = $query
        ->latest()
        ->where('status', 1)
        ->get();
        
        //to do: optimize the sortby with raw query
        if(!empty($sortBy))
            $ads = $ads->sortBy($sortBy[0], $sortBy[1]);

        $total = $ads->count();
        $perPage = 5;
        $page  = Paginator::resolveCurrentPage('page') ?: 1;
        $items = $ads->forPage($page, $perPage);

        return new LengthAwarePaginator($items, $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => 'page',
        ]);

    }
}

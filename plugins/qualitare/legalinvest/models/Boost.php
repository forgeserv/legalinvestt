<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class Boost extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_boosts';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
}

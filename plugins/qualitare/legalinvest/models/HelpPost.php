<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class HelpPost extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $fillable = ['help_category_id'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_help_posts';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'category' => [
            'Qualitare\LegalInvest\Models\HelpCategory', 
            'key' => 'help_category_id'
        ]
    ];

    /**
     * Filter by category
     * 
     */
    public function scopeByCategory($query, $categorySlug)
    {
        //to do: see eloquent relationships
        return $query
        ->select('title', 'qualitare_legalinvest_help_posts.slug as url', 'qualitare_legalinvest_help_categories.name as category' )
        ->join('qualitare_legalinvest_help_categories', 'qualitare_legalinvest_help_categories.id', '=', 'help_category_id')
        ->where("qualitare_legalinvest_help_categories.slug", $categorySlug);
    }

    public function scopeMostViewed($query)
    {
        //to do: see eloquent relationships
        return $query
        ->select('title', 'qualitare_legalinvest_help_posts.slug as url', 'qualitare_legalinvest_help_categories.slug as category' )
        ->join('qualitare_legalinvest_help_categories', 'qualitare_legalinvest_help_categories.id', '=', 'help_category_id')
        ->orderBy('views', 'desc')
        ->limit(5);
    }
    
    public function search($filters){
        
        $query = $this->newQuery();
        
        if($filters){

            if(!empty($filters['where'])){

                $where = $filters['where'];

                if(!empty($where['term'])){
                    $query->where(function ($query) use ($where) {
                        $query->where('title', 'LIKE', "%". $where['term'] . "%")
                            ->orWhere('text',  'LIKE', "%". $where['term'] . "%");
                    });
                }
            }   
        }

        return $query->orderBy('title', 'desc')->paginate(5);

    }

}

<?php

namespace Qualitare\LegalInvest\Models;

use Model;
use Auth;

/**
 * Message Models
 */
class Message extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_messages';

    public $timestamps = false;

    public $dates = ['created_at', 'updated_at'];
    
    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
	protected $fillable = [
		'content',
        'user_id',
        'conversation_id',
        'created_at',
        'updated_at'
	];

    /**
     * @var array Relations
     */
	public $belongsTo = [
		'user' => 'Qualitare\LegalInvest\Models\User'
    ];
    
    public static function markAsReadByConversation($conversationId){   

        return Message::where([
            ['conversation_id', $conversationId],
            ['user_id', '<>', Auth::getUser()->id]
        ])
        ->whereNull('updated_at')
        ->update(['updated_at' => now()]);
    }
}

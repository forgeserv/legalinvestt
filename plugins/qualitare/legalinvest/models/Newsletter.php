<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class Newsletter extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_newsletter';

    /**
     * @var array Validation rules
     */
    public $rules = [
        'email' => 'required|unique:qualitare_legalinvest_newsletter|email'
    ];

    public $fillable = ['email'];
    
    protected $dates = ['created_at'];

    public $timestamps = false;
}

<?php namespace Qualitare\LegalInvest\Models;


use Model;
use Qualitare\LegalInvest\Services\PaymentService;

/**
 * Model
 */
class Order extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_orders';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $belongsTo = [
        'user' => [
            'Qualitare\LegalInvest\Models\User', 
            'key' => 'user_id'
        ],
        'ad' => [
            'Qualitare\LegalInvest\Models\Ad', 
            'key' => 'ad_id'
        ],
        'boost' => [
            'Qualitare\LegalInvest\Models\Boost', 
            'key' => 'boost_id'
        ]
    ];

    public $fillable = ['user_id', 'ad_id', 'boost_id', 'reference', 'gateway', 'status'];

    public function isPaid()
    {
        $service = new PaymentService($this->gateway);
        $gateway = $service->getGateway();
        
        return $gateway->resolveStatus($this->status) == 'accepted';
    }

    public function isCanceled()
    {
        $service = new PaymentService($this->gateway);
        $gateway = $service->getGateway();
        
        return $gateway->resolveStatus($this->status) == 'canceled';
    }
}

<?php namespace Qualitare\LegalInvest\Models;

use Model;

/**
 * Model
 */
class AdCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_legalinvest_ad_categories';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    public $hasMany = [
        'ads' => 'Qualitare\LegalInvest\Models\Ad'
    ];

    public $attachOne = [
        'icon' => 'System\Models\File'
    ];

    /**
     * Show only parent categories 
     */
    public function scopeParentCategories($query, $categorySlug)
    {
        return $query
        ->where("parent_id", 0)
        ->orderBy("name", "asc");
    }
}

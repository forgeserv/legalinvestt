<?php namespace Qualitare\LegalInvest\Models;

use RainLab\User\Models\User as Model;

/**
 * Model
 */
class User extends Model
{
    public static $extraFillable = ["birthday", "cpf", "phone"];

    public static function extending($model){

        self::setHasMany($model);
        self::setExtraFillable($model);
        self::setCustomAppends($model);
        self::addMutators($model);
        self::setRules($model);
        self::setTranslatedAttributeNames($model);
        
    }

    public static function setHasMany($model){
        $model->hasMany['user_favorites'] = 'Qualitare\LegalInvest\Models\UserFavorite';
        $model->hasMany['ads']            = 'Qualitare\LegalInvest\Models\Ad';
        $model->hasMany['conversations']  = [
            'Qualitare\LegalInvest\Models\Conversation', 
            'key' => 'owner_id', 
            'other_key' => 'user_id'
        ];
    }

    public static function setExtraFillable($model){
        $model->addFillable(self::$extraFillable);
    }

    public static function setCustomAppends($model){
        $model->appends = ['fullname'];
    }

    public static function addMutators($model){

        //fullname 
        $model->addDynamicMethod('getFullnameAttribute', function($value) use ($model) {
            return $model->name . " " . $model->surname;
        });

        //cpf
        $model->addDynamicMethod('getCpfAttribute', function($value) use ($model) {
            if($value){
                $pattern = '/^([0-9]{3}.?)([0-9]{3}.?)([0-9]{3}-?)([0-9]{2})/';
                $replacement = '${1}.${2}.${3}-${4}';
                return trim(preg_replace($pattern, $replacement, $value));
            }

            return $model->attributes['cpf'];
        });

        $model->addDynamicMethod('setCpfAttribute', function($value) use ($model) {
            $model->attributes['cpf'] = trim(preg_replace('#[^0-9]#', '', $value));
        });

        //phone
        $model->addDynamicMethod('getPhoneAttribute', function($value) use ($model) {
            if($value){
                return phone($value, 'BR')->formatForCountry('BR');
            }
        });
    }

    public static function setRules($model){
        $model->rules['name']     = 'required';
        $model->rules['surname']  = 'required';
        $model->rules['birthday'] = 'required|date';
        $model->rules['cpf']      = 'required|cpf';
        $model->rules['phone']    = 'required|phone:BR';
    }

    public static function setTranslatedAttributeNames($model)
    {
        $model->attributeNames['name']     = 'nome';
        $model->attributeNames['surname']  = 'sobrenome';
        $model->attributeNames['birthday'] = 'aniversário';
        $model->attributeNames['cpf'] = 'cpf';
        $model->attributeNames['phone']    = 'telefone';
    }
}

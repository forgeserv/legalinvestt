<?php namespace Qualitare\LegalInvest\Classes;

use Auth;
use Closure;
use Redirect;

class AuthMiddleware
{
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return Redirect::to('/entrar');
        }

        return $next($request);
    }
}

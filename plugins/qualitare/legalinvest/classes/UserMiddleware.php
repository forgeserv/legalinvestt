<?php namespace Qualitare\LegalInvest\Classes;

use Auth;
use Closure;
use Redirect;

class UserMiddleware
{
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            return Redirect::to('/conta/meus-dados');
        }

        return $next($request);
    }
}

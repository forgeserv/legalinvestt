<?php namespace Qualitare\LegalInvest\Classes;

use Auth;
use Cms\Classes\Content as ContentBase;

class ChatFilters extends ContentBase
{
    public static function chatDate($date){
        $date = $date->format("d/m/Y");
        if($date == now()->format("d/m/Y")){
            return "Hoje"; // Today
        }

        return $date;
    }

    public static function chatSide($message){

        $isFromLoggedUser = $message->user_id == Auth::getUser()->id;
        
        return $isFromLoggedUser ? "message-right" : "message-left";
    }
}
<?php namespace Qualitare\LegalInvest\Classes;

use Cms\Classes\Content as ContentBase;
use Qualitare\LegalInvest\Models\Ad as Ad;

class AdFilters extends ContentBase
{
    public static function statusClass($code){
        switch ($code){
            case 1: return "published";
            case 2: return "blocked";
            case 3: return "finished";
            case 4: return "finished";
            default: return "wait";
        }
    }

    public static function statusName($code){
        return Ad::getStatusNameByCode($code);
    }
}